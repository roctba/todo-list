# todo-list


## Project GUI
```
vue ui
```

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### TODO
Fix: Error: [vuex] do not mutate vuex store state outside mutation handlers.