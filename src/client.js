import axios from 'axios';

let client;

export function getCancelToken() {
  return axios.CancelToken;
}

export function getClient(customOpts) {
  const defaultOpts = {
    baseURL: 'https://jsonplaceholder.typicode.com/todos',
    timeout: 50000,
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const fullOpts = Object.assign({}, defaultOpts, customOpts);

  client = axios.create(fullOpts);
  return client;
}
