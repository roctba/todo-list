import Vue from 'vue';
// import geClient from '../../client';
import { getClient } from '../../client';
// getCancelToken
export default {
  strict: true,
  namespaced: true,
  // -----------------------------------------------------------------
  state: {
    todos: null,
  },
  // -----------------------------------------------------------------
  getters: {
    todos: state => state.todos,
  },
  // -----------------------------------------------------------------
  mutations: {
    setTodos(state, todos) {
      Vue.set(state, 'todos', todos);
    },
    updateTodo(state, todo) {
      const idx = state.todos.findIndex(item => item.id === todo.id);
      // eslint-disable-next-line no-param-reassign
      // if (idx !== -1) state.todos[index] = todo;
      if (idx !== -1) state.todos.splice(idx, 1, todo);
    },
    addTodo(state, todo) {
      state.todos.unshift(todo);
    },
    removeTodo(state, todo) {
      const index = state.todos.findIndex(item => item.id === todo.id);
      // this.$delete(state.todos, index);
      state.todos.splice(index, 1);
    },
  },
  // -----------------------------------------------------------------
  actions: {
    setTodos(context, todos) {
      context.commit('setTodos', todos);
    },
    getTodos(context) {
      console.log('Store - getTodos');

      return getClient().get('/').then((response) => {
        context.commit('setTodos', response.data);
        return response.data;
      });
    },
    addTodo(context, todo) {
      console.log(`Store - addTodo ${JSON.stringify(todo)}`);
      return getClient().post('/', todo)
        .then((response) => {
          const savedTodo = JSON.parse(JSON.stringify(response.data));
          savedTodo.userId = 1;
          context.commit('addTodo', savedTodo);
          return savedTodo;
        });
    },
    updateTodo(context, todo) {
      console.log(`Store - updateTodo ${JSON.stringify(todo)}`);
      return getClient().put(`/${todo.id}`, todo)
        .then((resp) => {
          console.log(`Store - todoUpdated ${JSON.stringify(resp.data)}`);
          context.commit('updateTodo', todo);
          return todo;
        });
    },
    removeTodo(context, todo) {
      console.log(`Store - removeTodo ${JSON.stringify(todo)}`);

      return getClient().delete(`/${todo.id}`)
        .then(() => {
          context.commit('removeTodo', todo);
          return todo;
        });
    },
  },
};
